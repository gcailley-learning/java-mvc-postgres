# Java Mvc Postgres

# Installation tips


## How to run the project
```
mvn spring-boot:run
```
to auto reload the project after changes :
- Update Compile Preferences
    - From the “Intellij” 
    - menu at the top of your IDE, select “Preferences” and a window should appear. 
    - Search for “Compile”, or find compile in “Build, Execution, Deployment” -> “Compiler”
    - Select the checkbox for “Build Project Automatically” and select Apply.

## How to Activate Lombok on IntelliJ ?
- Go to File > Settings > Plugins.
- Click on Browse repositories...
- Search for Lombok Plugin.
- Click on Install plugin.
- Restart IntelliJ IDEA.

# Postgres DataBase

## How to create postgres local DB using Docker
```
docker-compose up
```

```
docker-compose run postgres bash
```

```
psql --host=postgres --username=postgres --dbname=hygee
```