package com.cbp.hygee.builder;

import com.cbp.hygee.models.Assure;

public class AssureBuilder {
    public static Assure create(
            String name,
            String phone,
            String email ) {
        return new Assure(null, name, phone, email);
    }
}
