package com.cbp.hygee.builder;

import com.cbp.hygee.models.Assure;
import com.cbp.hygee.models.DossierSante;

public class DossierSanteBuilder {
    public static DossierSante create(String description, Assure assure) {
        return new DossierSante(null, description, assure);
    }
}
