package com.cbp.hygee.controllers;


import com.cbp.hygee.models.DossierSante;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import com.cbp.hygee.dto.DossierSanteDTO;

import com.cbp.hygee.services.DossierSanteService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j(topic = "DossierSante")
@RestController
@RequestMapping("/secure/dossiers-sante")
public class DossierSanteController {

    @Autowired
    private DossierSanteService dossierSanteService;

    /**
     * Mapper MODELE <-> DTO.
     */
    private final ModelMapper modelMapper = new ModelMapper();


    @GetMapping
    public List<DossierSanteDTO> fetchDossiersSante(Optional<String> start, Optional<String> limit) {

        // convert to DTO
        return dossierSanteService.fetchAll(start, limit)
                .map(ds -> modelMapper.map(ds, DossierSanteDTO.class)).collect(Collectors.toList());
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public DossierSanteDTO persistDossierSante(@RequestBody DossierSanteDTO dossierSanteDTO) {

        DossierSante newDossierSante = modelMapper.map(dossierSanteDTO, DossierSante.class);
        dossierSanteService.persist(newDossierSante);

        return modelMapper.map(newDossierSante, DossierSanteDTO.class);
    }
}
