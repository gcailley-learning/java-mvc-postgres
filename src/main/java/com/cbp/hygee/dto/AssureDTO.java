package com.cbp.hygee.dto;

import lombok.*;
import lombok.experimental.FieldDefaults;

@FieldDefaults(level = AccessLevel.PRIVATE)
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString(of = { "name"})
public class AssureDTO {
    String name;
    String phone;
    String email;
}
