package com.cbp.hygee.health;

import com.cbp.hygee.builder.AssureBuilder;
import com.cbp.hygee.dto.AssureDTO;
import com.cbp.hygee.models.Assure;
import com.cbp.hygee.services.TpaService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.net.Socket;
import java.util.HashMap;
import java.util.Map;

@Component
@Slf4j
public class UrlShortenerServiceHealthIndicator
        implements HealthIndicator {

    @Autowired
    private TpaService tpaService;

    @Override
    public Health health() {

        // check if url shortener service url is reachable
        try {
            tpaService.isAlive();
        } catch (Exception e) {
            log.warn("Failed to connect to: {}", "URL");
            return Health.down()
                    .withDetail("error", e.getMessage())
                    .build();
        }
        return Health.up().build();
    }

}