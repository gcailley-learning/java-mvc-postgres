package com.cbp.hygee.models;

import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;


@FieldDefaults(level = AccessLevel.PRIVATE)
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString(of = {"id", "name"})
@Entity
@Cacheable
public class Assure {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
    String name;
    String phone;
    String email;
}
