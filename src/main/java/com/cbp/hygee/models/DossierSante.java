package com.cbp.hygee.models;

import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;

@FieldDefaults(level = AccessLevel.PRIVATE)
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString(of = {"description", "assure"})
@Entity
@Cacheable
public class DossierSante {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @Column(length = 40)
    String description;

    @OneToOne(cascade = {CascadeType.ALL})
    Assure assure;
}
