package com.cbp.hygee.repository;

import com.cbp.hygee.models.DossierSante;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DossierSanteRepository extends JpaRepository<DossierSante, String> {

}
