package com.cbp.hygee.security.controller;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import com.cbp.hygee.security.dto.User;
import com.cbp.hygee.security.filter.JWTAuthorizationFilter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.web.bind.annotation.*;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@Slf4j(topic = "Authentication")
@RestController
@RequestMapping("/api/auth")
public class UserController {

	private User auth = null;

	@PostMapping()
	public User login(@RequestParam("username") String username, @RequestParam("password") String pwd) {
		
		String token = getJWTToken(username);
		User user = new User();
		user.setUser(username);
		user.setToken(token);
		auth = user;
		return user;
		
	}

	@DeleteMapping()
	@ResponseStatus(HttpStatus.ACCEPTED)
	public void logout() {
		auth = null;
	}

	@GetMapping()
	@ResponseStatus(HttpStatus.ACCEPTED)
	public User currentUserName(HttpServletRequest request, HttpServletResponse response) throws IOException {
		if (null != auth) {
			return auth;
		} else {
			JWTAuthorizationFilter.unauthorizedResponse(response, "Not logged in");
			return null;
		}
	}
	private String getJWTToken(String username) {
		String secretKey = "mySecretKey";
		List<GrantedAuthority> grantedAuthorities = AuthorityUtils
				.commaSeparatedStringToAuthorityList("ROLE_USER");
		
		String token = Jwts
				.builder()
				.setId("softtekJWT")
				.setSubject(username)
				.claim("authorities",
						grantedAuthorities.stream()
								.map(GrantedAuthority::getAuthority)
								.collect(Collectors.toList()))
				.setIssuedAt(new Date(System.currentTimeMillis()))
				.setExpiration(new Date(System.currentTimeMillis() + 600000))
				.signWith(SignatureAlgorithm.HS512,
						secretKey.getBytes()).compact();

		return "Bearer " + token;
	}
}
