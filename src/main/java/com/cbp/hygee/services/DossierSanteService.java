package com.cbp.hygee.services;

import com.cbp.hygee.models.DossierSante;

import java.util.Optional;
import java.util.stream.Stream;

public interface DossierSanteService {

    public Stream<DossierSante> fetchAll(Optional<String> start, Optional<String> limit);

    /**
     * Save the "Dossier Sante" Object.
     * @param newDossierSante DossierSante to save
     */
   public DossierSante persist(DossierSante newDossierSante);
}
