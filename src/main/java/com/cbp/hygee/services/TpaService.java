package com.cbp.hygee.services;

import java.util.List;

public interface TpaService {
    public boolean isAlive() throws  Exception;
    public String getNews();
}
