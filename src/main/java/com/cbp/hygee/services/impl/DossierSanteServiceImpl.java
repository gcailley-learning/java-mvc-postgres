package com.cbp.hygee.services.impl;


import com.cbp.hygee.models.DossierSante;
import com.cbp.hygee.repository.DossierSanteRepository;
import com.cbp.hygee.services.DossierSanteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

@Service
public class DossierSanteServiceImpl implements DossierSanteService {

    @Autowired
    private DossierSanteRepository dossierSanteRepository;

    @Override
    public Stream<DossierSante> fetchAll(Optional<String> start, Optional<String> limit) {
        List<DossierSante> ds = dossierSanteRepository.findAll();
        return ds.stream();
    }

    @Override
    public DossierSante persist(DossierSante newDossierSante) {
        return dossierSanteRepository.save(newDossierSante);
    }
}
