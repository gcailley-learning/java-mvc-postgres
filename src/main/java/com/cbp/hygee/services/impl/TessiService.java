package com.cbp.hygee.services.impl;

import com.cbp.hygee.services.TpaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class TessiService implements TpaService {
    public static String URL_NEWS = "https://api.kfan.fr/public/index.php/association/news?latest=true";

    @Autowired
    RestTemplate restTemplate;

    @Override
    public boolean isAlive() throws Exception {
        ResponseEntity<String> response = restTemplate.getForEntity(URL_NEWS, String.class);
        if (!response.getStatusCode().is2xxSuccessful()) {
            throw new Exception("server is not alive");
        } else {
            return true;
        }
    }

    @Override
    public String getNews() {
        ResponseEntity<String> response = restTemplate.getForEntity(URL_NEWS, String.class);
        return response.getBody();
    }
}
