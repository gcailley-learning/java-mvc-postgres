package com.cbp.hygee.services.impl;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

@SpringBootTest
public class TessiServiceImplTest {

    @Mock
    private RestTemplate restTemplate;

    @InjectMocks
    private TessiService tessiService = new TessiService();

    @Test
    public void testIsAlive() throws Exception {
        Mockito
                .when(restTemplate.getForEntity(
                        TessiService.URL_NEWS, String.class))
          .thenReturn(new ResponseEntity<String>("ok", HttpStatus.ACCEPTED));

        Assertions.assertEquals(true, tessiService.isAlive());
    }

    @Test
    public void testIsNotAlive_404() throws Exception {
        Mockito
                .when(restTemplate.getForEntity(
                        TessiService.URL_NEWS, String.class))
                .thenReturn(new ResponseEntity<String>("not found", HttpStatus.NOT_FOUND));
        Assertions.assertThrows(Exception.class, () -> {
            tessiService.isAlive();
        });
    }

    @Test
    public void testIsNotAlive_Exception() throws Exception {
        Mockito
                .when(restTemplate.getForEntity(
                        TessiService.URL_NEWS, String.class))
                .thenThrow(org.springframework.web.client.RestClientException.class);
        Assertions.assertThrows(Exception.class, () -> {
            tessiService.isAlive();
        });
    }
}
